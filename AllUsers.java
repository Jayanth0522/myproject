package com.innominds.tinder;
/**
 * This is the  Implementation of the AllUsers class
 * @author jpalepally1
 *
 */
public class AllUsers {

	public String	 name;					// Represents the User Name
	public int   	 age;					// Represents the User Age
	public String 	 Profession;			// Represents the User Profession
	public String	 Passion;				// Represents the User Passion
	
	// Creation of a Constructor
	public void CreatenewBlundrUser(String name,int  age, String Profession, String Passion) {
		this.name		 = name;
		this.age 		 = age;
		this.Profession  = Profession;
		this.Passion	 = Passion;
		System.out.println(this);
	}
		 public String toString() {

				String stringToReturn = "";			
				
				stringToReturn    +="               Display the user details             \n";
				stringToReturn    +="=================================================="+"\n";
				stringToReturn   += "name       :                   " + this.name           +   "\n";
				stringToReturn   += "age        :                	" + this.age            +   "\n";
				stringToReturn   += "Profession :                   " + this.Profession     +   "\n";
				
				System.out.println("\n");
			return  stringToReturn;
		
	}

	
}
